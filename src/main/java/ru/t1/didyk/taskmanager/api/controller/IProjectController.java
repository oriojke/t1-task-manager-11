package ru.t1.didyk.taskmanager.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
